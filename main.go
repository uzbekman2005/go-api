package main

import (
	"fmt"
	users "go-api/Users"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func HelloWorld(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello world")
}

func HandleRequests() {
	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc("/", HelloWorld).Methods("GET")
	myRouter.HandleFunc("/users", users.AllUsers).Methods("GET")
	myRouter.HandleFunc("/user/{name}/{email}", users.NewUser).Methods("POST")
	myRouter.HandleFunc("/user/{name}", users.DeleteUser).Methods("DELETE")
	myRouter.HandleFunc("/user/{name}/{email}", users.UpdateUser).Methods("PUT")
	log.Fatal(http.ListenAndServe(":8081", myRouter))
}

func main() {
	users.InitialMigrations()

	HandleRequests()
}
